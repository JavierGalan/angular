import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FakeApiService {

  constructor(private fakeApiRequest:HttpClient) { }

  public getData(part: string){
    return this.fakeApiRequest.get(`${environment.baseUrl}/${part}`)
  }

}
