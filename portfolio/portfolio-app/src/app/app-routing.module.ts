import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', redirectTo: 'home', pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: ()=> import('./pages/home-page/home-page.module').then((m)=>m.HomePageModule)
  },
  {
    path: 'proyects',
    loadChildren: ()=> import('./pages/proyects-page/proyects-page.module').then((m)=>m.ProyectsPageModule)
  },
  {
    path: 'about',
    loadChildren: ()=> import('./pages/about-page/about-page.module').then((m)=>m.AboutPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
