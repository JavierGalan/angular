import { Iimg } from "src/app/models/iglobal";

export interface Iheader {
    logo: Iimg;
    navbar: Ilink[];
}

export interface Ilink{
    name: string;
    navigate:string;
}
