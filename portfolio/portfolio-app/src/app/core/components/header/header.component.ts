import { Component, OnInit } from '@angular/core';
import { FakeApiService } from 'src/app/services/fake-api.service';
import { Iheader } from './models/iheader';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public header?: Iheader

  constructor(private fakeApiService: FakeApiService) { }

  ngOnInit(): void {
    this.getHeader();
  }

  private getHeader(){
    this.fakeApiService.getData('header').subscribe(data =>{
      const results: any = data;
      this.header = results;
    });
  }

  // mostrar nav
  public showNav(){
    const nav = document.getElementById('navbar');
    if (nav!.style.display === 'flex') {
      nav!.style.display = 'none';
    }else{
      nav!.style.display = 'flex'
    }
  }
  // cerrar nav al pulsar navegacion
  public closeNav(){
    const nav = document.getElementById('navbar');
      nav!.style.display = 'none';
  }

}
