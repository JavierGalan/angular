import { Component, OnInit } from '@angular/core';
import { FakeApiService } from 'src/app/services/fake-api.service';
import { Ifooter } from './models/ifooter';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public footer?:Ifooter;

  constructor(private fakeApiRequest: FakeApiService) { }

  ngOnInit(): void {
    this.getFooter();
  }

  public getFooter(){
    this.fakeApiRequest.getData('footer').subscribe(data => {
      const results:any = data;
      this.footer = results;
    });
  }

}
