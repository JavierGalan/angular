import { Iimg } from "src/app/models/iglobal";

export interface Ifooter {
    copyright:String;
    rrss: Irrss[];
}

export interface  Irrss{
    name:string;
    img: Iimg;
}
