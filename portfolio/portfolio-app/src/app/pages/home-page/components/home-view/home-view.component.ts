import { Component, OnInit } from '@angular/core';
import { FakeApiService } from 'src/app/services/fake-api.service';
import { Ihome } from '../../models/ihome';

@Component({
  selector: 'app-home-view',
  templateUrl: './home-view.component.html',
  styleUrls: ['./home-view.component.scss']
})
export class HomeViewComponent implements OnInit {

  public home?:Ihome;

  constructor(private fakeApiRequest: FakeApiService) { }

  ngOnInit(): void {
    this.getHome();
  }

  public getHome(){
    this.fakeApiRequest.getData('home').subscribe(data => {
      const results:any = data
      this.home = results;
    });
  }

}
