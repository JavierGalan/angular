import { Iimg } from "src/app/models/iglobal";

export interface Ihome {
    title: string;
    proyectGallery: IproyectGallery[];
    tec:{
        title: string;
        imgs: Iimg[];
    }
}

export interface IproyectGallery{
    proyectTitle:string;
    img:Iimg;
}
