import { Iimg } from "src/app/models/iglobal";

export interface Iproyects {
}

export interface Iproyect{
    title:string;
    imgs:Iimg[];
    description:string[];
}