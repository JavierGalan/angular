import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProyectViewComponent } from './components/proyect-view/proyect-view.component';

const routes: Routes = [
  {
    path: '', component: ProyectViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProyectsPageRoutingModule { }
