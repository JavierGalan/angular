import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProyectsPageRoutingModule } from './proyects-page-routing.module';
import { ProyectViewComponent } from './components/proyect-view/proyect-view.component';


@NgModule({
  declarations: [
    ProyectViewComponent
  ],
  imports: [
    CommonModule,
    ProyectsPageRoutingModule
  ]
})
export class ProyectsPageModule { }
