import { Component, OnInit } from '@angular/core';
import { FakeApiService } from 'src/app/services/fake-api.service';
import { Iproyect } from '../../models/iproyects';

@Component({
  selector: 'app-proyect-view',
  templateUrl: './proyect-view.component.html',
  styleUrls: ['./proyect-view.component.scss']
})
export class ProyectViewComponent implements OnInit {

  public proyectView?:Iproyect;

  constructor(private fakeApiRequest: FakeApiService) {}

  ngOnInit(): void {}

  public showProyect($event:string){
    if($event === "guru"){
      this.fakeApiRequest.getData('proyects').subscribe(data => {
        const results:any = data;
        this.proyectView = results.guru;
      });
    }else if($event === "bombeito"){
      this.fakeApiRequest.getData('proyects').subscribe(data => {
        const results:any = data;
        this.proyectView = results.bombeos;
      });
    }
    console.log(this.proyectView);
  }

}
