import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectViewComponent } from './proyect-view.component';

describe('ProyectViewComponent', () => {
  let component: ProyectViewComponent;
  let fixture: ComponentFixture<ProyectViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProyectViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
